<?php
?>
  <div class="comment<?php print ' '. $status; ?>">
    <?php if ($picture) {
    print $picture;
  } ?>
<h3 class="title"><?php print $title; ?></h3><?php if ($new != '') { ?><span class="new"><?php print pluralism_mark(MARK_NEW); ?></span><?php } ?>
    <div class="content">
     <?php print $content; ?>
     <?php if ($signature): ?>
      <div class="clear-block">
       <div>—</div>
       <?php print $signature ?>
      </div>
     <?php endif; ?>
    </div>

    <div class="meta">
    <?php if ($submitted) { ?><div class="submitted"><?php print $submitted?></div><?php }; ?>
    <?php if ($links) { ?><div class="links"><?php print $links?></div><?php }; ?>
    <div style="clear: both;"> </div>
    </div>

  </div>
